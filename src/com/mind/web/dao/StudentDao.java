package com.mind.web.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;

import javax.sql.DataSource;

import com.mind.web.jdbc.entity.Student;


public class StudentDao {
	
	private DataSource dataSource;
	 
	public StudentDao(DataSource theDataSource){
		dataSource=theDataSource;
	}
	public List<Student> getStudents() throws SQLException {
		List<Student> students=new ArrayList<>();
		
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;
		try{
			myConn = dataSource.getConnection();
			
			// Step 3:  Create a SQL statements
			String sql = "select * from student";
			myStmt = myConn.createStatement();
			
			// Step 4:  Execute SQL query
			myRs = myStmt.executeQuery(sql);
			
			while (myRs.next()) {
				int id=myRs.getInt("id");
				String firstName=myRs.getString("first_name");
				String lastName=myRs.getString("last_name");
				String email=myRs.getString("email");
				
				Student tempStudent=new Student(id,firstName,lastName,email);
				
				students.add(tempStudent);
			}
			return students;
		}
		
		finally {
			close(myConn,myStmt,myRs);
		}
		
		
	}
	private void close(Connection myConn, Statement myStmt, ResultSet myRs) {
		try{
			if(myRs!=null){
				myRs.close();
			}
			if(myStmt!=null){
				myStmt.close();
			}
			if(myConn!=null){
				myConn.close();
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	public void addStudent(Student theStudent) throws SQLException {
		
		Connection myConn=null;
		PreparedStatement myStmt = null;
		
		try{
			// get db connection
			myConn=dataSource.getConnection();
			String sql = "insert into student "
					   + "(first_name, last_name, email) "
					   + "values (?, ?, ?)";
			
			myStmt = myConn.prepareStatement(sql);
			
			// set the param values for the student
			myStmt.setString(1, theStudent.getFirstName());
			myStmt.setString(2, theStudent.getLastName());
			myStmt.setString(3, theStudent.getEmail());
			
			// execute sql insert
			myStmt.execute();
			
		}
		finally {
			// clean up JDBC objects
			close(myConn, myStmt, null);
		}
		
	}
	public Student getStudent(String theStudentId) throws SQLException {
		
		Connection myConn=null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		int studentId;
		
		Student theStudent=null;
		
		try{
			studentId=Integer.parseInt(theStudentId);
			myConn=dataSource.getConnection();
			String sql="select * from student where id=?";
			myStmt=myConn.prepareStatement(sql);
			myStmt.setInt(1, studentId);
			myRs=myStmt.executeQuery();
			if(myRs.next()){
				String firstName=myRs.getString("first_name");
				String lastName=myRs.getString("last_name");
				String email=myRs.getString("email");
				
				theStudent=new Student(studentId,firstName,lastName,email);
			}
			else{
				throw new RuntimeException("Can Not Find StudentId"+theStudentId);
			}
			return theStudent;
			
		}
		finally{
			close(myConn, myStmt, myRs);
			
		}
		
		
		
		
	}
	public void updateStudent(Student theStudent) throws SQLException {
		
		Connection myConn=null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		
		try{
			myConn = dataSource.getConnection();
			
			// create SQL update statement
			String sql = "update student "
						+ "set first_name=?, last_name=?, email=? "
						+ "where id=?";
			
			// prepare statement
			myStmt = myConn.prepareStatement(sql);
			
			// set params
			myStmt.setString(1, theStudent.getFirstName());
			System.out.println("FirstName is  "+ theStudent.getFirstName());
			
			myStmt.setString(2, theStudent.getLastName());
			System.out.println("Last Name is "+theStudent.getLastName());
			
			myStmt.setString(3, theStudent.getEmail());
			
			System.out.println("Email is "+theStudent.getEmail());
			myStmt.setInt(4, theStudent.getId());
			
			System.out.println("Befor Executing Stmt");
			
			
			// execute SQL statement
			myStmt.execute();
			System.out.println("After Execution Stmt");
		}
		finally{
			close(myConn, myStmt, myRs);
		}
		
	
		
	}
	public void deleteStudent(String theStudentId) throws SQLException {
		Connection myConn=null;
		PreparedStatement myStmt = null;
		
		try{
			
			int studentId=Integer.parseInt(theStudentId);
			myConn=dataSource.getConnection();
			String sql="delete from student where id=?";
			myStmt=myConn.prepareStatement(sql);
			myStmt.setInt(1, studentId);
			myStmt.execute();
			
		}
		finally{
			close(myConn, myStmt, null);
			
		}
		
	}

}
